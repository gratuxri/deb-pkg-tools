coloredlogs>=14.0
executor>=22.0
humanfriendly>=8.2
property-manager>=3.0
six>=1.10.0

[memcached]
python-memcached>=1.58
